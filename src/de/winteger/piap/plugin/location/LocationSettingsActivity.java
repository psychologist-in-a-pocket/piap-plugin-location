package de.winteger.piap.plugin.location;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import de.uvwxy.daisy.common.location.GPSWIFIReader;
import de.uvwxy.daisy.common.location.LocationReader.LocationResultCallback;
import de.uvwxy.daisy.common.location.LocationReader.LocationStatusCallback;
import de.winteger.piap.plugin.location.R;

/**
 * Activity to set up the users home location
 * 
 * @author sarah
 * 
 */
public class LocationSettingsActivity extends Activity {

	private static final String TAG = "LoggerLocation";
	private EditText etLatitude;
	private EditText etLongitude;
	private Button btnSave;
	private Button btnShowMap;
	private Button btnCurLocation;
	private TextView tvLocationUpdateInfo;
	String latitudeString;
	String longitudeString;

	private GPSWIFIReader locationReader;
	private final boolean useGPS = true;
	private final boolean useWiFi = true;
	private final float requiredAccuracy = 30.0f;
	private final int timeOutSeconds = 60;

	private LocationStatusCallback cbStatus = new LocationStatusCallback() {

		@Override
		public void status(final Location l) {
			if (l == null) {
				return;
			}
			updateInfo(String.format("Current accuracy %.2f m > %.2f m, waiting for updates...", l.getAccuracy(), requiredAccuracy));
			Runnable r = new Runnable() {

				@Override
				public void run() {
					etLatitude.setText("" + l.getLatitude());
					etLongitude.setText("" + l.getLongitude());
				}
			};

			runOnUiThread(r);
		}
	};
	private LocationResultCallback cbResult = new LocationResultCallback() {

		@Override
		public void result(final Location l) {
			if (l == null) {
				updateInfo("Error reading location");
			}
			updateInfo(String.format(
					"Current accuracy is %.2f m < %.2f m, or we have timed out after %d seconds. Stopped location updates. Press again to refresh.",
					l.getAccuracy(), requiredAccuracy, timeOutSeconds));

			Runnable r = new Runnable() {

				@Override
				public void run() {
					etLatitude.setText("" + l.getLatitude());
					etLongitude.setText("" + l.getLongitude());
				}
			};

			runOnUiThread(r);

		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location_settings);

		etLatitude = (EditText) findViewById(R.id.etLatitude);
		etLongitude = (EditText) findViewById(R.id.etLongitude);
		tvLocationUpdateInfo = (TextView) findViewById(R.id.tvLocationUpdateInfo);

		btnSave = (Button) findViewById(R.id.btnSave);
		btnSave.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// read out data from edit texts and store it
				latitudeString = etLatitude.getText().toString();
				longitudeString = etLongitude.getText().toString();

				// parse Double and check for valid entrys
				Double latitude = Double.parseDouble(latitudeString);
				Double longitude = Double.parseDouble(longitudeString);

				if ((latitude >= -90) && (latitude <= 90) && (longitude >= -180) && (longitude <= 180)) {
					GlobalSettings.setLatitude(getApplicationContext(), latitudeString);
					GlobalSettings.setLongitude(getApplicationContext(), longitudeString);

					Toast.makeText(getApplicationContext(), R.string.updated_home_location_, Toast.LENGTH_SHORT).show();
					// user saved the location
					// return to settings in main app and update proximity alert (via resultcode)
					setResult(Activity.RESULT_OK);
					finish();
				} else {
					Toast.makeText(getApplicationContext(), R.string.this_is_not_a_valid_location_, Toast.LENGTH_SHORT).show();
				}

			}
		});

		btnShowMap = (Button) findViewById(R.id.btnShowMap);
		btnShowMap.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				String label = getString(R.string.home);

				latitudeString = etLatitude.getText().toString();
				longitudeString = etLongitude.getText().toString();

				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + latitudeString + "," + longitudeString + "?q=" + latitudeString + ","
						+ longitudeString + "(" + label + ")"));
				startActivity(i);
			}
		});

		btnCurLocation = (Button) findViewById(R.id.btnCurLocation);
		btnCurLocation.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				updateInfo(String.format("Will wait for accuracy better than %.2f m, or timeout after %d seconds", requiredAccuracy, timeOutSeconds));

				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				locationReader = new GPSWIFIReader(getApplicationContext(), timeOutSeconds * 1000, requiredAccuracy, cbStatus, cbResult, useGPS, useWiFi);
				locationReader.startReading();

			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (etLatitude.getText().toString().length() < 1) {
			etLatitude.setText(GlobalSettings.getLatitude(getApplicationContext()));
			etLongitude.setText(GlobalSettings.getLongitude(getApplicationContext()));
		}
	}

	@Override
	protected void onPause() {
		if (locationReader != null) {
			locationReader.stopReading();
		}
		super.onPause();
	}

	private void updateInfo(final String s) {
		if (s == null) {
			return;
		}

		Runnable r = new Runnable() {

			@Override
			public void run() {
				tvLocationUpdateInfo.setText(s);
			}
		};

		this.runOnUiThread(r);
	}

}
