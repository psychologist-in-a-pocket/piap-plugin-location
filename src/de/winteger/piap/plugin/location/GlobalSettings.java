package de.winteger.piap.plugin.location;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Access class for shared preferences
 * 
 * @author sarah
 * 
 */
public class GlobalSettings {

	// actions for intents
	public static final String LOCATION_ALERT = "de.winteger.piap.plugin.location.LOCATION_ALERT";
	public static final String LOCATION_ALERT_ADD = "de.winteger.piap.plugin.location.LOCATION_ALERT_ADD";
	public static final String LOCATION_ALERT_REMOVE = "de.winteger.piap.plugin.location.LOCATION_ALERT_REMOVE";
	public static final String LOCATION_ALERT_RELAY = "de.winteger.piap.plugin.location.LOCATION_ALERT_RELAY";

	// fields for shared preferences
	private static final String LATITUDE = "latitude";
	private static final String LONGITUDE = "longitude";
	private static final String PREF_ID = "MyPreferences";

	/**
	 * Returns a double value latitude as String
	 * 
	 * @param context
	 *            The context
	 * @return latitude
	 */
	public static String getLatitude(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREF_ID, 0);
		String savedLatitude = settings.getString(LATITUDE, "0");
		return savedLatitude;
	}

	/**
	 * Stores a longitude value
	 * 
	 * @param context
	 *            The context
	 * @param latitude
	 *            The longitude
	 */
	public static void setLatitude(Context context, String latitude) {
		SharedPreferences settings = context.getSharedPreferences(PREF_ID, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(LATITUDE, latitude);
		editor.commit();
	}

	/**
	 * Returns a double value longitude as String
	 * 
	 * @param context
	 *            The context
	 * @return longitude
	 */
	public static String getLongitude(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREF_ID, 0);
		String savedLongitude = settings.getString(LONGITUDE, "0");
		return savedLongitude;
	}

	/**
	 * Stores a latitude value
	 * 
	 * @param context
	 *            The context
	 * @param longitude
	 *            The latitude
	 */
	public static void setLongitude(Context context, String longitude) {
		SharedPreferences settings = context.getSharedPreferences(PREF_ID, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(LONGITUDE, longitude);
		editor.commit();
	}
}
