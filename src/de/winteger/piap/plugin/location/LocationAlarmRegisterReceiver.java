package de.winteger.piap.plugin.location;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;
import de.winteger.piap.plugin.location.R;

/**
 * This BroadcastReceiver receives broadcasts to registers and deregisters
 * proximity alerts
 * 
 * @author sarah
 * 
 */
public class LocationAlarmRegisterReceiver extends BroadcastReceiver {

	private static final String TAG = "LoggerLocation";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "received intent");

		// Start (add) or stop (remove) location logging?
		boolean add = false;
		if (intent.getAction().equals(GlobalSettings.LOCATION_ALERT_ADD)) {
			add = true;
		} else if (intent.getAction().equals(GlobalSettings.LOCATION_ALERT_REMOVE)) {
			add = false;
		} else if (intent.getAction().equals(GlobalSettings.LOCATION_ALERT)) {
			Intent i = new Intent(GlobalSettings.LOCATION_ALERT_RELAY);

			boolean entering = intent.getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING, false);

			i.putExtra(LocationManager.KEY_PROXIMITY_ENTERING, entering);
			context.sendBroadcast(i);
			return;
		}

		double latitude = Double.parseDouble(GlobalSettings.getLatitude(context));
		double longitude = Double.parseDouble(GlobalSettings.getLongitude(context));
		// let's try 50 meter...
		float radius = 50f;

		// Get location manger and setup logging
		LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

		// main PiaP app listens for this pending intent and handles it
		PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, new Intent(GlobalSettings.LOCATION_ALERT), 0);
		// add or remove proximity alert with this intent
		if (add) {
			// remove possible old proximityAlerts
			// could be a new location or something
			lm.removeProximityAlert(pIntent);
			Log.d(TAG, "removed old one");
			// add a new one
			// -1 = proximityAlert never expires
			lm.addProximityAlert(latitude, longitude, radius, -1, pIntent);
			// TODO: maybe in try catch?
			Toast.makeText(context, R.string.started_location_logging_, Toast.LENGTH_SHORT).show();
			Log.d(TAG, "added proximity alert for " + latitude + ", " + longitude);
			// Test
			//context.sendBroadcast(new Intent(GlobalSettings.LOCATION_ALERT));
		} else {
			// remove proximityAlert
			lm.removeProximityAlert(pIntent);
			// TODO: maybe in try catch?
			Toast.makeText(context, R.string.stopped_location_logging_, Toast.LENGTH_SHORT).show();
			Log.d(TAG, "removed proximity alert");
		}
	}

}
